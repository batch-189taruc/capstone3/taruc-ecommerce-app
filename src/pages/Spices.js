import {Fragment, useEffect, useState} from 'react';

import Spicesea from '../components/Spicesea';
export default function Spices() {

	const [spices, setSpices] = useState([])

	useEffect(() => {
		fetch(`https://immense-peak-78641.herokuapp.com/spices`)
		.then(res => res.json())
		.then(data => {
			setSpices(data.map(spice =>{
				return (
					<Spicesea key={spice._id} spiceProp={spice}/>

					)

			}))
		})


	}, [])

	return(

			
			<Fragment>
				<h1>Spices</h1>
				{spices}
			</Fragment>
		)


}