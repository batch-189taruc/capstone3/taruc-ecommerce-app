import {useState, useEffect, useContext } from 'react'; //s54 activity
import {Form, Button} from 'react-bootstrap';
import {useParams, useNavigate} from 'react-router-dom'
//import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function UpdateProduct(){
	const {spiceId} = useParams()

//const {user} = useContext(UserContext)
	const[name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState (0);
	const  [stock, setStock] = useState (0)

let navigate = useNavigate()
function routeChange1(){
	let path =`/allspices`;
		navigate(path);
}

function updateProduct(e){
e.preventDefault()
					fetch(`https://immense-peak-78641.herokuapp.com/spices/updateproduct/${spiceId}`, {
					method: "PUT",
					headers: {
						'Content-type': 'application/json',
						Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
					name: name,
					description: description,
					price: price,
					stock: stock,
					
					})

				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					if (data === true){
						Swal.fire({
							title: "Success!",
							icon: "success",
							text: "New spice added!"
						})
						
						//navigate("/allspices");
					}else{
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Try again!"
						})
						//navigate("/allspices");
					}
				})
			}

useEffect(() => {
		fetch(`https://immense-peak-78641.herokuapp.com/users/allspices/${spiceId}`, {
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=> res.json())
		.then(data =>{
			
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setStock(data.stock)
			
		})
	}, [spiceId])

	return(
		<Form>
			<h1 className="text-center">Update {name} data</h1>
			 <Form.Group className="mb-3" controlId="name">
		        <Form.Label>Name</Form.Label>
		        <Form.Control 
		        		type="text" 
		        		placeholder="Enter new name"
		        		value={name}
		        		onChange={e => {setName(e.target.value)}}
		        		required />
		      </Form.Group>
		     
		      <Form.Group className="mb-3" controlId="description">
		        <Form.Label>Description:</Form.Label>
		        <Form.Control 
		        		type="textarea" 
		        		placeholder="Enter new description"
		        		value={description}
		        		onChange={e => {setDescription(e.target.value)}}
		        		required />
		        
		      </Form.Group>
		      <Form.Group className="mb-3" controlId="price">
		        <Form.Label>Price</Form.Label>
		        <Form.Control 
		        		type="number" 
		        		placeholder="Set a new price"
		        		value={price}
		        		onChange={e => {setPrice(e.target.value)}}
		        		required />
		      </Form.Group>
		       <Form.Group className="mb-3" controlId="stock">
		        <Form.Label>Stock Number</Form.Label>
		        <Form.Control 
		        		type="number" 
		        		placeholder="Set a new stock quantity"
		        		value={stock}
		        		onChange={e => {setStock(e.target.value)}}
		        		required />
		      </Form.Group>
		      <Button variant="danger" onClick={() => {routeChange1()}}>Back</Button>
		      <Button variant="primary" onClick={(e) => {updateProduct(e);routeChange1()}}>Save</Button>
		      
		      </Form>

		)
}