import {useState, useEffect, useContext} from 'react';
import { Card, Button } from 'react-bootstrap';
import {useParams, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext'
import AllOrdersea from '../components/AllOrdersea'


export default function AdminProductView(){

	const {user} = useContext(UserContext)

	//The "useParams"
	const {spiceId} = useParams();
	const[name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState (0);
	const [isActive, setIsActive] = useState ("");
	const [stock, setStock] = useState ("");
	const [createdOn, setCreatedOn] = useState ("");

	const [theOrders, setTheOrders] = useState ("");


let navigate = useNavigate()
function routeChange(){
	let path =`/updateproduct/${spiceId}`;
		navigate(path);
}
function routetolist(){
	let path1 =`/allspices`;
		navigate(path1);
}


function deleteBtn(){
	fetch(`https://immense-peak-78641.herokuapp.com/spices/deleteproduct/${spiceId}`,{
			method: "DELETE",
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		}).then(res=> alert(`${name} has been deleted!`))
}

function archiveBtn(){
	fetch(`https://immense-peak-78641.herokuapp.com/spices/archive/${spiceId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		}).then(res=> alert(`${name} has been archived!`))
}

function unArchiveBtn(){
	fetch(`https://immense-peak-78641.herokuapp.com/spices/unarchive/${spiceId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		}).then(res=> alert(`${name} is now active!`))
}
	

	useEffect(() => {
		fetch(`https://immense-peak-78641.herokuapp.com/users/allspices/${spiceId}`, {
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=> res.json())
		.then(data =>{
			
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setIsActive(data.isActive)
			setStock(data.stock)
			setCreatedOn(data.createdOn)
		})
	}, [spiceId]);

	useEffect(()=>{
		fetch(`https://immense-peak-78641.herokuapp.com/spices/vieworders/${spiceId}`,{
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data =>{
			

			
			setTheOrders(data.map(theOrders =>{
				return(
					<AllOrdersea key= {theOrders._id} theOrdersProp={theOrders}/>)
			}))
		})
	}, [spiceId])


	return(
		
		<>
		
			<Card>
					<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>{spiceId}</Card.Subtitle>
					<Card.Text>Description: {description}</Card.Text>
					<Card.Text>Price: Php {price}</Card.Text>
					<Card.Text>Stock: {stock}</Card.Text>
					<Card.Text>Active status: {String(isActive)}</Card.Text>
					<Card.Text>Listed date: {createdOn}</Card.Text>
					<Button variant="primary" onClick={() => {routeChange()}}>Update Product data</Button>
					{
						(isActive === true) ?
						<Button variant="success" onClick ={() => {archiveBtn(); routetolist()}}>Archive</Button>
						:
						<Button variant="secondary" onClick ={() => {unArchiveBtn(); routetolist()}}>Unarchive</Button>
									}
					<Button variant="danger" onClick={() => {deleteBtn();routetolist()}}>Delete</Button>
                

                
      
    </Card.Body>
					</Card>

					<Card>
					<Card.Body>
					<Card.Title> Current Orders</Card.Title>
					<Card.Text>{theOrders}</Card.Text>
					</Card.Body>
					</Card>
					
					
		</>

		)
}