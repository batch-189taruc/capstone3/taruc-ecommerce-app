import {useState, useEffect } from 'react';
import {Container} from 'react-bootstrap';
import {UserProvider} from './UserContext'
import AppNavbar from './components/AppNavbar';
import {BrowserRouter as Router} from 'react-router-dom';
import {Routes, Route} from 'react-router-dom';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';
import Spices from './pages/Spices';
import SpiceView from './pages/SpiceView'
import MyOrders from './pages/MyOrders'
import AdminHome from './pages/AdminHome'
import AllUsers from './pages/AllUsers'
import AllSpices from './pages/AllSpices'
import AdminUserView from './pages/AdminUserView'
import AdminSpiceView from './pages/AdminSpiceView'
import UpdateProduct from './pages/UpdateProduct'
import AddProduct from './pages/AddProduct'
import About from './pages/About'



import './App.css';

function App(){

//hook for global user state
const [user, setUser] = useState({
  id: null,
  isAdmin: null,
  address: null
})
//Clearing local storage @logout
const unsetUser = () =>{
  localStorage.clear()
};

//
useEffect(()=> {
  fetch('https://immense-peak-78641.herokuapp.com/users/details',{
    headers:{
      Authorization: `Bearer ${localStorage.getItem('token')}`
    }
  })
.then(res => res.json())
.then(data =>{
  if(typeof data._id !== "undefined"){
    setUser({
      id: data._id,
      isAdmin: data.isAdmin,
      address: data.address
    })
  }else{
    setUser({
      id: null,
      isAdmin: null,
      address: null
    })
  }
})
}, [])

  return(
    <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar/>
            <Container>
              <Routes>
        <Route path="/" element={<Home/>}/>
        <Route path="/spices" element={<Spices/>}/> {/*catalog view*/}
        <Route path="/spices/:spiceId" element={<SpiceView/>}/>
        <Route path="/login" element={<Login/>}/>
        <Route path="/logout" element={<Logout/>}/>
        <Route path="/register" element={<Register/>}/>
        <Route path="*" element={<NotFound/>}/>
        <Route path="/adminhome" element={<AdminHome/>}/>
        <Route path="/allusers" element={<AllUsers/>}/>
        <Route path="/allspices" element={<AllSpices/>}/>
        <Route path="/myorders" element={<MyOrders/>}/>
        <Route path="/allusers/:userId" element={<AdminUserView/>}/>
        <Route path="/allspices/:spiceId" element={<AdminSpiceView/>}/>
        <Route path="/updateproduct/:spiceId" element={<UpdateProduct/>}/>
         <Route path="/addproduct" element={<AddProduct/>}/>
         <Route path="/about" element={<About/>}/>
         
    

        

              </Routes>
          </Container>
        </Router>
   </UserProvider>


    )
}

export default App;