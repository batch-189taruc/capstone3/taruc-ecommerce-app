import {Fragment, useContext} from 'react';
import {Navbar, Nav, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext'

export default function AppNavbar(){
  const {user} = useContext(UserContext);

return( (user.isAdmin === false) ?
    <Navbar id="navbar-bg" variant="light" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to="/">LOGO</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="justify-content-end">
            {
              (user.id !== null) ?
              <>
                <Nav.Link id="nav-text" as={Link} to="/">Home</Nav.Link>
                <Nav.Link id="nav-text" as={Link} to="/spices">Spices</Nav.Link>
                <Nav.Link id="nav-text" as={Link} to="/myorders">MyCart</Nav.Link>
                <Nav.Link id="nav-text" as={Link} to="/logout">Logout</Nav.Link>
                
                
                </>
                :
                <>
                <Nav.Link id="nav-text" as={Link} to="/">Home</Nav.Link>
                <Nav.Link id="nav-text" as={Link} to="/spices">Spices</Nav.Link>
                <Nav.Link id="nav-text" as={Link} to="/login">Login</Nav.Link>
                <Nav.Link id="nav-text" as={Link} to="/register">Register</Nav.Link>
                <Nav.Link id="nav-text" as={Link} to="/about">About</Nav.Link>
                
                </>
            }
           
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
    :
    <Navbar id="navbar-bg" variant="light" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to="/">LOGO</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="justify-content-end">
            
             {
              (user.id !== null) ?
              <>
                <Nav.Link id="nav-text" as={Link} to="/allusers">All Users</Nav.Link>
                <Nav.Link id="nav-text" as={Link} to="/allspices">All Spices</Nav.Link>
                {/*<Nav.Link as={Link} to="/allorders">All Orders</Nav.Link>*/}
                <Nav.Link id="nav-text" as={Link} to="/logout">Logout</Nav.Link>

                </>
                :
                <>
                <Nav.Link id="nav-text" as={Link} to="/">Home</Nav.Link>
                <Nav.Link id="nav-text" as={Link} to="/spices">Spices</Nav.Link>
                <Nav.Link id="nav-text" as={Link} to="/login">Login</Nav.Link>
                <Nav.Link id="nav-text" as={Link} to="/register">Register</Nav.Link>
                <Nav.Link id="nav-text" as={Link} to="/about">About</Nav.Link>
                </>
            }
           
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
		)
}