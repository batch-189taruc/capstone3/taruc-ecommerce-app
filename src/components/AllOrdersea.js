import {Card} from 'react-bootstrap'


//deconstructed
export default function AllOrdersea({theOrdersProp}){
	
	const { userId, username, quantity, subtotal, isPaid, orderTime} = theOrdersProp;
	
	
	return(
			
				
					<Card>
					<Card.Body>
					<Card.Title>{username}</Card.Title>
					<Card.Subtitle>User ID: {userId}</Card.Subtitle>
					<Card.Text>Quantity: {quantity}</Card.Text>
					<Card.Text>Subtotal: {subtotal}</Card.Text>
					<Card.Text>Paid status: {String(isPaid)}</Card.Text>
					<Card.Text>Listed date: {orderTime}</Card.Text>
					
					</Card.Body>
					</Card>
					
			

		)

}