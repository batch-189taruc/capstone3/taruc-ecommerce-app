import {Row, Col, Button, Container} from 'react-bootstrap'
import { useNavigate} from 'react-router-dom';

export default function Banner(){

let navigate = useNavigate();

function routeChange(){
	let path =`/spices`;
		navigate(path);
}
	return(
		<Container id="cont-img">
		<Row>
			<Col className="p-5">
				<h1 id="banner-text">Spice Market</h1>
				<p id="banner-text">Shop spice online!</p>
				<Button variant="primary" onClick ={() => {routeChange()}}>See our spices!</Button>
			</Col>
		</Row>
		</Container>

		)
}